import React, { useEffect, useState } from 'react';

function App() {
  const [data, setData] = useState();
  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch('http://localhost:5000/api/data');
      const text = await response.text();
      
      // delay 1s before update dom
      setTimeout(() => {
        setData(text);
      }, 1000);
    };

    fetchData();
  }, []);
  const display = `fetching data: ${data ?? '...'}`

  return (
    <div>
      <h1>{display}</h1>
    </div>
  );
}
export default App;