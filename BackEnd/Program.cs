var builder = WebApplication.CreateBuilder(args);
// builder.Services.AddCors(options =>
// {
//     options.AddDefaultPolicy(policy  =>
//                         {
//                             policy.WithOrigins("http://localhost:3000",
//                                                 "http://localhost:3001");
//                         });
// });

var app = builder.Build();

// app.UseCors();

app.MapGet("/api/data", () =>
{
    // simulate api
    return "mock data from api";
});

app.Run();
