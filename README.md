## start backend .net core webapi

1. jump to correct folder
`cd Backend`

2. restore the library
`dotnet restore`

3. start backend: Now listening on: http://localhost:5000
`dotnet run`


## start font end

1. restore the library must be setup the nodejs
`npm i`

2. start react fontend: Now listening on: http://localhost:3000

### `npm start`
